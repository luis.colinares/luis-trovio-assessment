Luis Colinares - Trovio Technical Assessment

BACKGROUND
This is a Java-based test suite utilising the REST-Assured library and JUnit testing framework to test the contact list app https://thinking-tester-contact-list.herokuapp.com/


PREREQUISITES
1. Install Java https://www.java.com/en/download/
2. Install Maven https://maven.apache.org/

INSTRUCTIONS
1. Ensure all prerequisites have been installed.
2. Navigate to the project folder in the terminal.
3. Run the command 'mvn test'.
4. View the test results directly in the terminal, or the.xml files in the surefire-reports folder for more details.

FUTURE IMPROVEMENTS
- As this is just a POC, only a few basic tests were included to demonstrate the test suite. 
- It is currently split into User-Based tests and Contact-Based tests, however ideally coverage would include regression/negative tests/smoke tests/happy paths/performance etc. based on the testing needs of the business.
- A separate json file for test data would also be preferred, to keep the test java files shorter and more concise.
- Test results could be better improved with more details and visualisations, using other third-party tools such as JIRA and SmartBear.