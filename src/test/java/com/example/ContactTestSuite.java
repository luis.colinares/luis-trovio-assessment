import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;

public class ContactTestSuite {

    private static final String BASE_URL = "https://thinking-tester-contact-list.herokuapp.com";
    private static String authToken;

    @BeforeClass
    public void setup() {
        RestAssured.baseURI = BASE_URL;
        authToken = obtainAuthToken("john.tester@test.com", "john123123");
    }
    
    @Test
    public void testGetContacts() {
        given()
            .header("Authorization", "Bearer " + authToken)
            .when()
            .get("/contacts")
            .then()
            .statusCode(200)
            .body("size()", greaterThan(0));
    }

    @Test
    public void testAddContact() {
        String requestBody = "{\n" +
                "    \"firstName\": \"John\",\n" +
                "    \"lastName\": \"Doe\",\n" +
                "    \"birthdate\": \"1970-01-01\",\n" +
                "    \"email\": \"jdoe@fake.com\",\n" +
                "    \"phone\": \"8005555555\",\n" +
                "    \"street1\": \"1 Main St.\",\n" +
                "    \"street2\": \"Apartment A\",\n" +
                "    \"city\": \"Anytown\",\n" +
                "    \"stateProvince\": \"KS\",\n" +
                "    \"postalCode\": \"12345\",\n" +
                "    \"country\": \"USA\"\n" +
                "}";

        given()
            .contentType("application/json")
            .header("Authorization", "Bearer " + authToken)
            .body(requestBody)
        .when()
            .post(BASE_URL + "/contacts")
        .then()
            .statusCode(201);
    }

    private String obtainAuthToken(String email, String password) {
        Response response = given()
            .contentType("application/json")
            .body("{\"email\": \"" + email + "\", \"password\": \"" + password + "\"}")
            .when()
            .post("/users/login");

        return response.then()
            .statusCode(200)
            .extract()
            .path("token");
    }
}    