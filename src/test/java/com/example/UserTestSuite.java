import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;

public class UserTestSuite {

    private static final String BASE_URL = "https://thinking-tester-contact-list.herokuapp.com";
    private static String authToken;

    @BeforeClass
    public void setup() {
        RestAssured.baseURI = BASE_URL;
        authToken = obtainAuthToken("john.tester@test.com", "john123123");
    }

    @Test
    public void testBaseUrlAvailability() {
        given()
            .when()
                .get(BASE_URL)
            .then()
                .statusCode(200);
    }

    @Test
    public void testLoginUser() {

        String requestBody = "{\n" +
                "    \"email\": \"john.tester@test.com\",\n" +
                "    \"password\": \"john123123\"\n" + "}";
        given()
            .contentType("application/json")
            .body(requestBody)
        .when()
            .post(BASE_URL + "/users/login")
        .then()
            .statusCode(200);
    }



    private String obtainAuthToken(String email, String password) {
        Response response = given()
            .contentType("application/json")
            .body("{\"email\": \"" + email + "\", \"password\": \"" + password + "\"}")
            .when()
            .post("/users/login");

        return response.then()
            .statusCode(200)
            .extract()
            .path("token");
    }
}    